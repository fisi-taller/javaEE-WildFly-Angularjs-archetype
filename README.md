Maven archetype que genera una web app con tecnologias JavaEE (JPA, CDI, JAX-RS)
 y AngularJS, la comunicacion se hace a traves de servicios REST, usando como
servidor de aplicaciones JBoss WildFly.

Requisitos
==========
 - JDK 1.8
 - Maven 3.3.x
 - WildFly 10.1
 - Opcionalmente: Eclipse JEE Neon 2 con JBoss Tools 4.4.2

Instalacion del Artifacto y Generacion de Proyecto
==================================================
 - Bajar los codigos de este proyecto
 - cd /path/to/this/project
 - mvn install
 - cd /path/to/where/you/want/to/create/your/project
 - mvn archetype:generate -DarchetypeCatalog=local -DarchetypeGroupId=pe.edu.unmsm.fisi.taller -DarchetypeArtifactId=javaEE-WildFly-Angularjs
 
